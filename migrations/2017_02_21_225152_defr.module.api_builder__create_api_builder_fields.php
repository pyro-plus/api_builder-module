<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleApiBuilderCreateApiBuilderFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'name'             => 'anomaly.field_type.text',
        'route'            => [
            'type'   => 'anomaly.field_type.slug',
            'config' => [
                'slugify' => 'name',
                'type'    => '-',
            ],
        ],
        'stream_reference' => [
            'type'   => 'anomaly.field_type.select',
            'config' => [
                'handler' => 'Defr\ApiBuilderModule\Api\Handler\Streams@handle',
            ],
        ],
        'excluded'         => [
            'type'   => 'anomaly.field_type.checkboxes',
            'config' => [
                'mode'    => 'tags',
                'handler' => 'Defr\ApiBuilderModule\Api\Handler\Fields@handle',
            ],
        ],
        'allowed_roles'    => [
            'type'   => 'anomaly.field_type.multiple',
            'config' => [
                'related' => 'Anomaly\UsersModule\Role\RoleModel',
            ],
        ],
    ];

}
