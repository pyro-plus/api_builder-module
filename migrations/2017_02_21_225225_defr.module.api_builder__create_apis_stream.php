<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleApiBuilderCreateApisStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'apis',
        'title_column' => 'name',
        'trashable'    => true,
        'sortable'     => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name'             => ['required' => true],
        'stream_reference' => ['required' => true],
        'route'            => ['required' => true, 'unique' => true],
        'excluded',
        'allowed_roles',
    ];

}
