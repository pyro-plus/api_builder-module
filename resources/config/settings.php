<?php

return [
    'prefix' => [
        'required'    => true,
        'placeholder' => false,
        'type'        => 'anomaly.field_type.text',
        'config'      => [
            'default_value' => 'rest',
        ],
    ],
];
