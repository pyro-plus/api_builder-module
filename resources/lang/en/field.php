<?php

return [
    'name'             => [
        'name' => 'Name',
    ],
    'route'            => [
        'name' => 'Route',
    ],
    'stream_reference' => [
        'name' => 'Stream',
    ],
    'excluded'         => [
        'name'    => 'Excluded Properties',
        'warning' => 'Save before editing!!!',
    ],
    'allowed_roles'    => [
        'name' => 'Allowed Roles',
    ],
];
