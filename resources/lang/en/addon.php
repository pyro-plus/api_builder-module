<?php

return [
    'title'       => 'API Builder',
    'name'        => 'API Builder Module',
    'description' => '',
    'section'     => [
        'apis' => 'REST APIs',
    ],
];
