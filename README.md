# API Builder Module
## Streams Platform Addon. `api_builder-module` for PyroCMS.

A graphical user interface (GUI), allowing you to create mount points of REST API in PyroCMS, relating to it, selected stream.

![Selection_507.jpg](https://bitbucket.org/repo/4pppg47/images/2656212381-Selection_507.jpg)

### Features
- Creates API controller on-fly;
- Choose one stream for one API mount;
- Ability to exclude some fields;
    - TODO _AJAX load of fields might be excluded_
- Choose allowed roles.

***

## Download
Clone repository into `addons/{app_reference}/defr/api_builder-module` folder, or add this module to your PyroCMS project manually uploading files.

### Alternative way
Add to `composer.json`:
```js
    "require": {
    
        // ...,
        
        "defr/backup_manager-module": "dev-master"
    },
    
    "repositories": [
        
        // ...,
        
        {
            "url": "https://github.com/Piterden/backup_manager-module",
            "type": "git"
        }
    ],
```

Run `composer update` command.
This way add-ons would be installed into `core` folder.

***

## Installation
After placing files in correct place, you will need to install migrations using the PyroCMS Control Panel or simply run one of following commands:
```bash
$ php artisan module:install api_builder
```
or
```bash
$ php artisan addon:install defr.module.api_builder
```
A new link of menu will be appeared in your admin navigation.

***

## Usage

### Create API mount point from Control Panel
* Click the menu item "Add API".
* You should see form with next fields
    + **name**
| The name of a mount
    + **stream_reference** | Related Stream
    + **route** | Route URI
    + **excluded** | Excluded fields
    + **allowed_roles** | Allowed roles
* Press save and your API mount had be done.

***