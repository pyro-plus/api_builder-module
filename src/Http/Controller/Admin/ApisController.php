<?php namespace Defr\ApiBuilderModule\Http\Controller\Admin;

use Defr\ApiBuilderModule\Api\Form\ApiFormBuilder;
use Defr\ApiBuilderModule\Api\Table\ApiTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class ApisController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param ApiTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ApiTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param ApiFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ApiFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param ApiFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ApiFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
