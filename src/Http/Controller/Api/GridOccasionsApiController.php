<?php namespace Defr\ApiBuilderModule\Http\Controller\Api;

use Anomaly\GridModule\Occasion\Contract\OccasionRepositoryInterface;

class GridOccasionsApiController extends ApiController
{

    /**
     * Create new GridOccasionsApiController instance
     *
     * @param {$entity}RepositoryInterface  $repository  The repository
     */
    public function __construct(OccasionRepositoryInterface $repository)
    {
        parent::__construct($repository);
    }
}
