<?php namespace Defr\ApiBuilderModule\Http\Controller\Api;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;
use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;
use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Anomaly\Streams\Platform\Support\Collection;
use Defr\ApiBuilderModule\Api\Command\GetExcludedProperties;
use Illuminate\Http\Request;

class ApiController extends PublicController
{
    /**
     * Entry Repository
     *
     * @var EntryRepositoryInterface
     */
    protected $repository;

    /**
     * Create an instance of RestController
     *
     * @param EntryRepositoryInterface $repository [description]
     */
    public function __construct(EntryRepositoryInterface $repository)
    {
        parent::__construct();

        $this->repository = $repository;

        if (!$this->request->ajax())
        {
            return redirect('/');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!$collection = $this->repository->all())
        {
            return response()->json(collect([]), 400);
        }

        /* @var EntryInterface $entry */
        return response()->json($collection->map(function ($entry)
        {
            foreach ($entry->getRelations() as $slug => $relation)
            {
                if ($slug == 'entry')
                {
                    $assignmentFields = $relation->getAssignmentFieldSlugs();

                    if (count($assignmentFields))
                    {
                        foreach ($assignmentFields as $key)
                        {
                            $relation[$key.'_obj'] = $relation->getFieldValue($key)->get();
                        }
                    }

                    $entry->entry_obj = $relation;
                }

                if ($slug == 'translations')
                {
                    $entry->translations_obj = $relation->mapWithKeys(
                        function ($trans)
                        {
                            return [$trans->locale => $trans];
                        }
                    );
                }
            }

            $entry->setHidden(
                $this->dispatch(new GetExcludedProperties($entry))
            );

            return $entry;
        }
        ), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json(collect([]), 405);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request    $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response()->json(collect([]), 405);
    }

    /**
     * Display the specified resource.
     *
     * @param  int                         $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $entry = $this->repository->findBySlug($slug);

        if (!$entry)
        {
            return response()->json(collect([]), 400);
        }

        return response()->json($entry, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json(collect([]), 405);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request    $request
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return response()->json(collect([]), 405);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(collect([]), 405);
    }
}
