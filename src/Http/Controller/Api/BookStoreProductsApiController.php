<?php namespace Defr\ApiBuilderModule\Http\Controller\Api;

use Pixney\BookStoreModule\Product\Contract\ProductRepositoryInterface;

class BookStoreProductsApiController extends ApiController
{

    /**
     * Create new BookStoreProductsApiController instance
     *
     * @param {$entity}RepositoryInterface  $repository  The repository
     */
    public function __construct(ProductRepositoryInterface $repository)
    {
        parent::__construct($repository);
    }
}
