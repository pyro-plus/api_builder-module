<?php namespace Defr\ApiBuilderModule;

use Anomaly\SettingsModule\Setting\Command\GetSettingValue;
use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Anomaly\Streams\Platform\Model\ApiBuilder\ApiBuilderApisEntryModel;
use Defr\ApiBuilderModule\Api\ApiModel;
use Defr\ApiBuilderModule\Api\ApiRepository;
use Defr\ApiBuilderModule\Api\Command\WriteApiControllers;
use Defr\ApiBuilderModule\Api\Contract\ApiRepositoryInterface;
use Defr\ApiBuilderModule\Http\Middleware\Cors;
use Illuminate\Config\Repository;
use Illuminate\Routing\Router;

class ApiBuilderModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Addon bindings
     *
     * @var array
     */
    protected $bindings = [
        ApiBuilderApisEntryModel::class => ApiModel::class,
    ];

    /**
     * Addon singletons
     *
     * @var array
     */
    protected $singletons = [
        ApiRepositoryInterface::class => ApiRepository::class,
    ];

    /**
     * Addon route middleware
     *
     * @var array
     */
    protected $routeMiddleware = [
        'cors' => Cors::class,
    ];

    /**
     * Addon routes
     *
     * @var array
     */
    protected $routes = [
        'admin/api_builder'           => 'Defr\ApiBuilderModule\Http\Controller\Admin\ApisController@index',
        'admin/api_builder/create'    => 'Defr\ApiBuilderModule\Http\Controller\Admin\ApisController@create',
        'admin/api_builder/edit/{id}' => 'Defr\ApiBuilderModule\Http\Controller\Admin\ApisController@edit',
    ];

    /**
     * Map routes
     *
     * @param Router                 $router The router
     * @param Repository             $config The configuration
     * @param ApiRepositoryInterface $apis   The apis
     */
    public function map(
        Router $router,
        Repository $config,
        ApiRepositoryInterface $apis
    )
    {
        $this->dispatch(new WriteApiControllers());

        $router->group([
            'prefix'     => $this->dispatch(
                new GetSettingValue('defr.module.api_builder::prefix')
            ),
            'middleware' => ['cors'],
        ], function ($router) use ($apis)
        {
            $apis->all()->each(function ($api) use ($router)
            {
                $suffix    = $api->getSuffix();
                $namespace = ucfirst(camel_case($api->getStreamNamespace()));

                $class = "Defr\\ApiBuilderModule\\Http\\Controller\\Api\\{$namespace}{$suffix}ApiController";

                if (class_exists($class))
                {
                    $router->resource($api->getRoute(), $class);
                }
            });
        });
    }

}
