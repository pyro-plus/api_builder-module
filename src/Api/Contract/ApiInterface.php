<?php namespace Defr\ApiBuilderModule\Api\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

/**
 * Interface ApiInterface
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
interface ApiInterface extends EntryInterface
{

    /**
     * Gets the name.
     *
     * @return string The name.
     */
    public function getName();

    /**
     * Gets the route.
     *
     * @return string The route.
     */
    public function getRoute();

    /**
     * Gets the suffix.
     *
     * @return string The suffix.
     */
    public function getSuffix();

    /**
     * Gets the stream slug.
     *
     * @return string The slug.
     */
    public function getStreamSlug();

    /**
     * Gets excluded properties.
     *
     * @return array The excluded.
     */
    public function getExcluded();

    /**
     * Gets the stream namespace.
     *
     * @return string The namespace.
     */
    public function getStreamNamespace();

    /**
     * Gets the actions.
     *
     * @return array The actions.
     */
    public function getAllowedRoles();

}
