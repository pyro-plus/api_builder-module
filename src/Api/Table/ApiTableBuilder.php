<?php namespace Defr\ApiBuilderModule\Api\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

/**
 * Class ApiTableBuilder
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ApiTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        'name'             => [
            'value' => 'entry.edit_link',
            'href'  => 'admin/api_builder/edit/{id}',
        ],
        'route'            => [
            'value' => 'entry.route_label',
        ],
        'stream_reference' => [
            'value' => 'entry.stream_label',
        ],
        'allowed_roles'          => [
            'value' => 'entry.allowed_roles_label',
        ],
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit',
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete',
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [];

}
