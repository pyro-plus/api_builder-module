<?php namespace Defr\ApiBuilderModule\Api;

use Anomaly\Streams\Platform\Entry\EntryPresenter;
use Anomaly\SettingsModule\Setting\Command\GetSettingValue;

class ApiPresenter extends EntryPresenter
{

    /**
     * Gets stream label
     *
     * @return string
     */
    public function streamLabel()
    {
        $slug      = ucfirst($this->object->getStreamSlug());
        $namespace = ucfirst($this->object->getStreamNamespace());

        return $this->label("{$namespace} - {$slug}", 'info', 'pill');
    }

    /**
     * Gets API route label
     *
     * @return string
     */
    public function routeLabel()
    {
        $url    = url('/');
        $route  = $this->object->getRoute();
        $prefix = $this->dispatch(new GetSettingValue('defr.module.api_builder::prefix'));

        return "{$url}/{$prefix}/{$route}";
    }

    /**
     * Gets actions label
     *
     * @return string
     */
    public function allowedRolesLabel()
    {
        $roles = $this->object->getAllowedRoles();

        if (!$roles->count())
        {
            return $this->label('Anyone', 'success');
        }

        return $roles->map(function ($role)
        {
            $name = $role->getName();
            $type = 'success';

            if ($name == 'Admin')
            {
                $type = 'danger';
            }

            if ($name == 'User')
            {
                $type = 'warning';
            }

            return $this->label($name, $type);
        })
        ->implode(' ');
    }
}
