<?php namespace Defr\ApiBuilderModule\Api\Command;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;
use Defr\ApiBuilderModule\Api\Contract\ApiRepositoryInterface;

/**
 * Class GetExcludedProperties
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class GetExcludedProperties
{

    protected $entry;

    public function __construct(EntryInterface $entry)
    {
        $this->entry = $entry;
    }

    /**
     * Handle the command.
     *
     * @param Parser     $parser
     * @param Filesystem $filesystem
     */
    public function handle(ApiRepositoryInterface $apis)
    {
        if ($api = $apis->findBySlugAndNamespace(
            $this->entry->getStreamSlug(),
            $this->entry->getStreamNamespace()
        ))
        {
            return $api->getExcluded();
        }

        return [];
    }
}
