<?php namespace Defr\ApiBuilderModule\Api\Command;

use Anomaly\Streams\Platform\Addon\Addon;
use Anomaly\Streams\Platform\Addon\Command\GetAddon;
use Anomaly\Streams\Platform\Stream\Contract\StreamRepositoryInterface;
use Anomaly\Streams\Platform\Support\Locator;
use Anomaly\Streams\Platform\Support\Parser;
use Defr\ApiBuilderModule\ApiBuilderModule;
use Defr\ApiBuilderModule\Api\Contract\ApiRepositoryInterface;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Class WriteApiControllers
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class WriteApiControllers
{
    use DispatchesJobs;

    /**
     * Handle the command.
     *
     * @param StreamRepositoryInterface $streams    The streams
     * @param ApiRepositoryInterface    $apis       The apis
     * @param ApiBuilderModule          $addon      The addon
     * @param Filesystem                $filesystem The filesystem
     * @param Locator                   $locator    The locator
     * @param Parser                    $parser     The parser
     */
    public function handle(
        StreamRepositoryInterface $streams,
        ApiRepositoryInterface $apis,
        ApiBuilderModule $addon,
        Filesystem $filesystem,
        Locator $locator,
        Parser $parser
    )
    {
        $filesystem->delete(
            array_filter(
                $filesystem->files($addon->getPath('src/Http/Controller/Api')),
                function ($file)
                {
                    return basename($file) != 'ApiController.php';
                }
            )
        );

        $apis->all()->each(function ($api) use ($addon, $filesystem, $parser, $locator, $streams)
        {
            $apiAddon = $this->dispatch(new GetAddon($locator->locate(
                $streams->findBySlugAndNamespace(
                    array_get($api->getStreamReferenceArray(), 1),
                    array_get($api->getStreamReferenceArray(), 0)
                )->getEntryModel()
            )));

            $suffix = $api->getSuffix();
            $entity = str_singular($suffix);
            $stream = ucfirst(camel_case($api->getStreamNamespace()));
            $class  = "{$stream}{$suffix}ApiController";

            $vendor = ($apiAddon instanceof Addon)
                ? ucfirst($apiAddon->getVendor())
                : ucfirst($api->getVendor());

            $namespace = $addon->getTransformedClass('Http\\Controller\\Api');

            $path = $addon->getPath("src/Http/Controller/Api/{$class}.php");

            $template = $filesystem->get(
                $addon->getPath('resources/stubs/entity/http/controller/api.stub')
            );

            $filesystem->makeDirectory(dirname($path), 0755, true, true);

            $filesystem->put(
                $path,
                $parser->parse(
                    $template,
                    compact('class', 'namespace', 'suffix', 'stream', 'entity', 'vendor')
                )
            );
        });
    }
}
