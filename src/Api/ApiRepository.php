<?php namespace Defr\ApiBuilderModule\Api;

use Anomaly\Streams\Platform\Entry\EntryRepository;
use Defr\ApiBuilderModule\Api\Contract\ApiRepositoryInterface;

class ApiRepository extends EntryRepository implements ApiRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var ApiModel
     */
    protected $model;

    /**
     * Create a new ApiRepository instance.
     *
     * @param ApiModel $model
     */
    public function __construct(ApiModel $model)
    {
        $this->model = $model;
    }

    /**
     * Find api of stream by slug and namespace
     *
     * @param  string         $slug      The slug
     * @param  srting         $namespace The namespace
     * @return ApiInterface
     */
    public function findBySlugAndNamespace($slug, $namespace)
    {
        return $this->model
        ->where('stream_reference', $slug.'.'.$namespace)
        ->first();
    }
}
