<?php namespace Defr\ApiBuilderModule\Api;

use Anomaly\Streams\Platform\Addon\Command\GetAddon;
use Anomaly\Streams\Platform\Model\ApiBuilder\ApiBuilderApisEntryModel;
use Defr\ApiBuilderModule\Api\Contract\ApiInterface;

class ApiModel extends ApiBuilderApisEntryModel implements ApiInterface
{

    /**
     * Gets the name.
     *
     * @return string The name.
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the route.
     *
     * @return string The route.
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Gets the suffix.
     *
     * @return string The suffix.
     */
    public function getSuffix()
    {
        return ucfirst(camel_case($this->getStreamSlug()));
    }

    /**
     * Gets the stream slug.
     *
     * @return string The slug.
     */
    public function getStreamSlug()
    {
        return array_get($this->getStreamReferenceArray(), 1);
    }

    /**
     * Gets excluded properties.
     *
     * @return array The excluded.
     */
    public function getExcluded()
    {
        return $this->excluded;
    }

    /**
     * Gets the stream namespace.
     *
     * @return string The namespace.
     */
    public function getStreamNamespace()
    {
        return array_get($this->getStreamReferenceArray(), 0);
    }

    /**
     * Gets the stream reference array.
     *
     * @return array The stream reference array.
     */
    public function getStreamReferenceArray()
    {
        return explode('.', $this->stream_reference);
    }

    /**
     * Gets roles have access.
     *
     * @return RoleCollection Roles have access.
     */
    public function getAllowedRoles()
    {
        return $this->allowed_roles;
    }

    /**
     * Gets the addon.
     *
     * @return Addon The addon.
     */
    public function getAddon()
    {
        return $this->dispatch(new GetAddon($this->getStreamNamespace()));
    }

    /**
     * Gets the vendor.
     *
     * @return <type> The vendor.
     */
    public function getVendor()
    {
        return $this->getAddon()->getVendor();
    }
}
