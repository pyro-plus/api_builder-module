<?php namespace Defr\ApiBuilderModule\Api\Handler;

use Anomaly\CheckboxesFieldType\CheckboxesFieldType;
use Anomaly\Streams\Platform\Field\Contract\FieldRepositoryInterface;

/**
 * Class Fields
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class Fields
{

    /**
     * Handle the options.
     *
     * @param CheckboxesFieldType      $fieldType
     * @param FieldRepositoryInterface $fields      The fields
     */
    public function handle(
        CheckboxesFieldType $fieldType,
        FieldRepositoryInterface $fields
    )
    {
        $entry     = $fieldType->getEntry();
        $slug      = $entry->getStreamSlug();
        $namespace = $entry->getStreamNamespace();

        $fieldType->setOptions($fields->all()
            ->filter(function ($field) use ($namespace)
            {
                return $namespace == $field->getNamespace();
            })
            ->mapWithKeys(function ($field)
            {
                return [
                    $field->getSlug() => $field->getName(),
                ];
            })
            ->toArray()
        );
    }
}
