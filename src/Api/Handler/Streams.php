<?php namespace Defr\ApiBuilderModule\Api\Handler;

use Anomaly\SelectFieldType\SelectFieldType;
use Anomaly\Streams\Platform\Stream\Contract\StreamRepositoryInterface;

/**
 * Class Streams
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class Streams
{

    /**
     * Handle the options.
     *
     * @param SelectFieldType           $fieldType
     * @param StreamRepositoryInterface $streams     The streams
     */
    public function handle(
        SelectFieldType $fieldType,
        StreamRepositoryInterface $streams
    )
    {
        $fieldType->setOptions($streams->all()->mapWithKeys(function ($stream)
        {
            return [
                $stream->getNamespace().'.'.$stream->getSlug() => ucfirst($stream->getNamespace()).' - '.ucfirst($stream->getSlug()),
            ];
        }
        )->toArray());
    }
}
