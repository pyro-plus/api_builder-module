<?php namespace Defr\ApiBuilderModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

class ApiBuilderModule extends Module
{

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'fa fa-puzzle-piece';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'apis' => [
            'buttons' => [
                'new_api',
            ],
        ],
    ];
}
